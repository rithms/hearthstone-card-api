FORMAT: 1A
HOST: http://hearthapi.com/

# Hearth Card API

The Hearth Card API provides developers with updated and detailed card data from Blizzard Entertainment's popular online card game, Hearthstone. There is currently no [officially supported](https://dev.battle.net/io-docs) Hearthstone API by Blizzard.

"Hearth Card API" isn’t endorsed by Blizzard Entertainment and doesn’t reflect the views or opinions of Blizzard or anyone officially involved in producing or managing Hearthstone.
©2014 Blizzard Entertainment, Inc. All rights reserved. Heroes of Warcraft is a trademark, and Hearthstone is a registered trademark of Blizzard Entertainment, Inc. in the U.S. and/or other countries.

## Setup

There are no longer any public servers for this API. No worries, the project is open source. You are free to setup this API on your own private
server for your Hearthstone applications. :)

For further setup information, view the project README on GitHub.

## Responses

Some typical responses you can expect to receive from the API include:

| Code            | Message |
|:----------------- |:------------- |
| 200                | OK             |
| 400 | Bad Request          |
| 404 | Not Found           |
| 429 | Too Many Requests |
| 500 | Internal Server Error |

Except for 500, we hope you never get that. :)

## Rate Limit

The current rate limit is enforced:

Requests: 100 per minute / 5,000 per hour / 50,000 per day.

This rate limit should be generous enough, as most of the data is static.

## Constants

### Locale
This API supports multiple languages. To filter data by specific languages, you can set the optional locale query parameter.
The following languages are supported:
    
| Locale            | Language Code |
|:----------------- |:------------- |
| German - Germany        | deDE    |
| English - Great Britain      | enGB          |
| English - United States        | enUS          |
| Spanish - Spain        | esES          |
| Spanish - Mexico        | esMX          |
| French - France        | frFR          |
| Italian - Italy        | itIT          |
| Korean        | koKR          |
| Polish        | plPL          |
| Portuguese - Brazil        | ptBR          |
| Portuguese - Portugal        | ptPT          |
| Russian        | ruRU          |
| Chinese - China        | zhCN          |
| Chinese - Taiwan        | zhTW          |

Note: Values that haven't been translated will default to English.
    
    
### Card Sets
To filter card data by card sets, you can set the optional cardSet query parameter.
The following card sets are supported:
    
| Card Set            | 
|:----------------- |
| Basic |
| Classic |
| Reward |
| Missions |
| System |
| Debug |
| Promotion |
| Curse of Naxxramas |
| Goblin vs Gnomes |
| Blackrock Mountain |
| Credits |

### Card Type
To filter card data by card types, you can set the optional cardType query parameter.
The following card types are supported:
    
| Card Type            | 
|:----------------- |
| Hero |
| Minion |
| Spell |
| Enchantment |
| Weapon |
| Hero Power |

### Class
To filter card data by classes, you can set the optional class query parameter.
The following classes are supported:
    
| Class            | 
|:----------------- |
| Developer |
| Druid |
| Hunter |
| Mage |
| Paladin |
| Priest |
| Rogue |
| Shaman |
| Warlock |
| Warrior |
| Dream |

### Race
To filter card data by races, you can set the optional race query parameter.
The following races are supported:
    
| Race            | 
|:----------------- |
| Murloc |
| Demon |
| Mechanical |
| Beast |
| Totem |
| Pirate |
| Dragon |

### Faction
To filter card data by factions, you can set the optional faction query parameter.
The following factions are supported:
    
| Faction            | 
|:----------------- |
| Horde |
| Alliance |
| Neutral |

### Rarity
To filter card data by rarity, you can set the optional rarity query parameter.
The following card rarities are supported:
    
| Rarity            | 
|:----------------- |
| Developer |
| Common |
| Free |
| Rare |
| Epic |
| Legendary |


## Card Format

```json
{
    "CARD_NAME": {
        "health": int
        "attack": int
        "cost": int
        "cardSet": string
        "cardTextInHand": string
        "cardName": string
        "durability": int
        "class": string
        "race": string
        "faction": string
        "cardType": string
        "rarity": string
        "attackVisualType": int
        "cardTextInPlay": string
        "devState": int
        "targetingArrowText": string
        "enchantmentBirthVisual": int
        "enchantmentIdleVisual": int
        "artistName": string
        "flavorText": string
        "howToGetThisGoldCard": string
        "howToGetThisCard": string
        "image": string
        "goldImage": string
    }
}
```


## Group Card API


## Card Collection [/v1.0/cards/{?locale}{?dataById}{?cardType}{?cardSet}{?class}{?race}{?faction}{?rarity}{?collectible}{?elite}]

Get a collection of cards. 

You can filter by locale, cardType, cardSet, class, race, faction, and rarity using optional query parameters.
Returns all cards in English (locale: enUS) by default.

+ Parameters
    + locale: enUS (optional, string) - Locale code for returned data. Defaults to enUS.
        + Members
            + deDE
            + enGB
            + enUS
            + esES
            + esMX
            + frFR
            + itIT
            + koKR
            + plPL
            + ptBR
            + ptPT
            + ruRU
            + zhCN
            + zhTW
    + dataById (optional, boolean) - If true, the returned data map will use the card's ID as the key. If false or not specified, the card's name will be used as the key.
    + cardSet: Blackrock Mountain (optional, string) - Card set. If not specified, returns all sets.
        + Members
            + Basic
            + Classic
            + Reward
            + Missions
            + System
            + Debug
            + Promotion
            + Curse of Naxxramas
            + Goblin vs Gnomes
            + Blackrock Mountain
            + Credits
    + cardType (optional, string) - Card type. If not specified, returns all types.
        + Members
            + Hero
            + Minion
            + Spell
            + Enchantment
            + Weapon
            + Hero Power
    + rarity (optional, string) - Card rarity. If not specified, returns all rarities.
        + Members
            + Developer
            + Common
            + Free
            + Rare
            + Epic
            + Legendary
    + class (optional, string) - The class this card belongs to. If not specified, returns all classes.
        + Members
            + Developer
            + Druid
            + Hunter
            + Mage
            + Paladin
            + Priest
            + Rogue
            + Shaman
            + Warlock
            + Warrior
            + Dream
    + race (optional, string) - The race of this card. If not specified, returns cards from all races.
        + Members
            + Murloc
            + Demon
            + Mechanical
            + Beast
            + Totem
            + Pirate
            + Dragon
    + faction (optional, string) - The faction this card belongs to. If not specified, returns cards from all factions.
        + Members
            + Horde
            + Alliance
            + Neutral
    + collectible (optional, boolean) - If true, returns collectible cards. If false, returns non-collectible cards. If not specifed, returns both.
    + elite: true (optional, boolean) - If true, returns elite cards. If false, returns non-elite cards. If not specified, returns both.

### GET

+ Response 200 (application/json)

        {
           "Magmatron": {
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRMA14_9_premium.gif",
              "collectible": false,
              "mechanics": [
                 "Aura",
                 "Trigger Visual"
              ],
              "enchantmentIdleVisual": 0,
              "cardSet": "Blackrock Mountain",
              "cardName": "Magmatron",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRMA14_9.png",
              "enchantmentBirthVisual": 0,
              "cardType": "Minion",
              "rarity": "Legendary",
              "attack": 7,
              "race": "Mechanical",
              "health": 7,
              "cardTextInHand": "Whenever a player plays a card, Magmatron deals 2 damage to them.",
              "elite": true,
              "id": "BRMA14_9",
              "cost": 5
           },
           "Toxitron": {
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRMA14_5H_premium.gif",
              "collectible": false,
              "mechanics": [
                 "Trigger Visual"
              ],
              "enchantmentIdleVisual": 0,
              "cardSet": "Blackrock Mountain",
              "cardName": "Toxitron",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRMA14_5H.png",
              "enchantmentBirthVisual": 0,
              "cardType": "Minion",
              "rarity": "Legendary",
              "attack": 4,
              "race": "Mechanical",
              "health": 4,
              "cardTextInHand": "At the start of your turn, deal 1 damage to all other minions.",
              "elite": true,
              "id": "BRMA14_5H",
              "cost": 1
           },
           "Emperor Thaurissan": {
              "mechanics": [
                 "Trigger Visual"
              ],
              "cardName": "Emperor Thaurissan",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRM_028.png",
              "howToGetThisGoldCard": "Can be crafted after completing Blackrock Depths.",
              "cost": 6,
              "cardTextInHand": "At the end of your turn, reduce the Cost of cards in your hand by (1).",
              "id": "BRM_028",
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRM_028_premium.gif",
              "enchantmentIdleVisual": 0,
              "attack": 5,
              "health": 5,
              "collectible": true,
              "cardType": "Minion",
              "elite": true,
              "cardSet": "Blackrock Mountain",
              "enchantmentBirthVisual": 0,
              "howToGetThisCard": "Unlocked by completing Blackrock Depths.",
              "rarity": "Legendary",
              "artistName": "Wayne Reynolds",
              "flavorText": "His second greatest regret is summoning an evil Firelord who enslaved his entire people."
           },
           "Chromaggus": {
              "mechanics": [
                 "Trigger Visual"
              ],
              "cardName": "Chromaggus",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRM_031.png",
              "cardTextInHand": "Whenever you draw a card, put another copy into your hand.",
              "cost": 8,
              "howToGetThisGoldCard": "Can be crafted after completing Blackwing Lair.",
              "id": "BRM_031",
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRM_031_premium.gif",
              "enchantmentIdleVisual": 0,
              "attack": 6,
              "health": 8,
              "collectible": true,
              "cardType": "Minion",
              "elite": true,
              "flavorText": "Left head and right head can never agree about what to eat for dinner, so they always end up just eating ramen again.",
              "cardSet": "Blackrock Mountain",
              "enchantmentBirthVisual": 0,
              "howToGetThisCard": "Unlocked by completing Blackwing Lair.",
              "rarity": "Legendary",
              "artistName": "Todd Lockwood",
              "race": "Dragon"
           },
           "Rend Blackhand": {
              "mechanics": [
                 "Battlecry"
              ],
              "targetingArrowText": "Destroy a Legend.",
              "cardName": "Rend Blackhand",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRM_029.png",
              "cardTextInHand": "Battlecry: If you're holding a Dragon, destroy a Legendary minion.",
              "cost": 7,
              "howToGetThisGoldCard": "Can be crafted after completing Blackrock Spire.",
              "id": "BRM_029",
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRM_029_premium.gif",
              "enchantmentIdleVisual": 0,
              "attack": 8,
              "health": 4,
              "collectible": true,
              "cardType": "Minion",
              "elite": true,
              "flavorText": "Rend believes he is the True Warchief of the Horde and he keeps editing the wikipedia page for \"Warchief of the Horde\" to include his picture.",
              "cardSet": "Blackrock Mountain",
              "enchantmentBirthVisual": 0,
              "howToGetThisCard": "Unlocked by completing Blackrock Spire.",
              "rarity": "Legendary",
              "artistName": "Alex Horley"
           },
           "Electron": {
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRMA14_7_premium.gif",
              "collectible": false,
              "mechanics": [
                 "Aura"
              ],
              "enchantmentIdleVisual": 0,
              "cardSet": "Blackrock Mountain",
              "cardName": "Electron",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRMA14_7.png",
              "enchantmentBirthVisual": 0,
              "cardType": "Minion",
              "rarity": "Legendary",
              "attack": 5,
              "race": "Mechanical",
              "health": 5,
              "cardTextInHand": "All spells cost (3) less.",
              "elite": true,
              "id": "BRMA14_7",
              "cost": 3
           },
           "Majordomo Executus": {
              "mechanics": [
                 "Deathrattle"
              ],
              "cardName": "Majordomo Executus",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRM_027.png",
              "howToGetThisGoldCard": "Can be crafted after completing Molten Core.",
              "cost": 9,
              "cardTextInHand": "Deathrattle: Replace your hero with Ragnaros, the Firelord.",
              "id": "BRM_027",
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRM_027_premium.gif",
              "enchantmentIdleVisual": 0,
              "attack": 9,
              "health": 7,
              "collectible": true,
              "cardType": "Minion",
              "elite": true,
              "cardSet": "Blackrock Mountain",
              "enchantmentBirthVisual": 0,
              "howToGetThisCard": "Unlocked by completing Molten Core.",
              "rarity": "Legendary",
              "artistName": "Alex Horley Orlandelli",
              "flavorText": "You thought Executus turned you into Ragnaros, but really Ragnaros was in you the whole time."
           },
           "Arcanotron": {
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRMA14_3_premium.gif",
              "collectible": false,
              "mechanics": [
                 "Spell Power"
              ],
              "enchantmentIdleVisual": 0,
              "cardSet": "Blackrock Mountain",
              "cardName": "Arcanotron",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRMA14_3.png",
              "enchantmentBirthVisual": 0,
              "cardType": "Minion",
              "rarity": "Legendary",
              "attack": 2,
              "race": "Mechanical",
              "health": 2,
              "cardTextInHand": "Both players have Spell Damage +2.",
              "elite": true,
              "id": "BRMA14_3",
              "cost": 0
           },
           "Magmaw": {
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRMA14_12_premium.gif",
              "collectible": false,
              "mechanics": [
                 "Taunt"
              ],
              "enchantmentIdleVisual": 0,
              "cardSet": "Blackrock Mountain",
              "cardName": "Magmaw",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRMA14_12.png",
              "enchantmentBirthVisual": 0,
              "cardType": "Minion",
              "rarity": "Legendary",
              "attack": 10,
              "cost": 5,
              "health": 2,
              "cardTextInHand": "Taunt",
              "elite": true,
              "id": "BRMA14_12"
           },
           "Nefarian": {
              "mechanics": [
                 "Battlecry"
              ],
              "cardName": "Nefarian",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRM_030.png",
              "cardTextInHand": "Battlecry: Add 2 random spells to your hand (from your opponent's class).",
              "cost": 9,
              "howToGetThisGoldCard": "Can be crafted after completing the Hidden Laboratory.",
              "id": "BRM_030",
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRM_030_premium.gif",
              "enchantmentIdleVisual": 0,
              "attack": 8,
              "health": 8,
              "collectible": true,
              "cardType": "Minion",
              "elite": true,
              "flavorText": "They call him \"Blackwing\" because he's a black dragon...... and he's got wings.",
              "cardSet": "Blackrock Mountain",
              "enchantmentBirthVisual": 0,
              "howToGetThisCard": "Unlocked by defeating every boss in Blackrock Mountain!",
              "rarity": "Legendary",
              "artistName": "Ruan Jia",
              "race": "Dragon"
           },
           "Gyth": {
              "goldImage": "http://wow.zamimg.com/images/hearthstone/cards/enus/animated/BRMA09_5Ht_premium.gif",
              "collectible": false,
              "cost": 3,
              "cardSet": "Blackrock Mountain",
              "cardName": "Gyth",
              "locale": "enUS",
              "image": "http://wow.zamimg.com/images/hearthstone/cards/enus/original/BRMA09_5Ht.png",
              "cardType": "Minion",
              "rarity": "Legendary",
              "attack": 8,
              "race": "Dragon",
              "health": 8,
              "elite": true,
              "id": "BRMA09_5Ht"
           }
        }

## Card By Name [/v1.0/cards/by-name/{name}{?locale}{?cardType}{?cardSet}{?class}{?race}{?faction}{?rarity}{?collectible}{?elite}]

Get a card by its name. 

If multiple cards share the same name, multiple cards will be returned.

You can filter by locale, cardType, cardSet, class, race, faction, and rarity using optional query parameters.
Returns all cards in English (locale: enUS) by default.

+ Parameters
    + name: Angry Chicken (required, string) - Name of the card.
    + locale: enUS (optional, string) - Locale code for returned data. Defaults to enUS.
        + Members
            + deDE
            + enGB
            + enUS
            + esES
            + esMX
            + frFR
            + itIT
            + koKR
            + plPL
            + ptBR
            + ptPT
            + ruRU
            + zhCN
            + zhTW
    + cardSet (optional, string) - Card set. If not specified, returns all sets.
        + Members
            + Basic
            + Classic
            + Reward
            + Missions
            + System
            + Debug
            + Promotion
            + Curse of Naxxramas
            + Goblin vs Gnomes
            + Blackrock Mountain
            + Credits
    + cardType (optional, string) - Card type. If not specified, returns all types.
        + Members
            + Hero
            + Minion
            + Spell
            + Enchantment
            + Weapon
            + Hero Power
    + rarity (optional, string) - Card rarity. If not specified, returns all rarities.
        + Members
            + Developer
            + Common
            + Free
            + Rare
            + Epic
            + Legendary
    + class (optional, string) - The class this card belongs to. If not specified, returns all classes.
        + Members
            + Developer
            + Druid
            + Hunter
            + Mage
            + Paladin
            + Priest
            + Rogue
            + Shaman
            + Warlock
            + Warrior
            + Dream
    + race (optional, string) - The race of this card. If not specified, returns cards from all races.
        + Members
            + Murloc
            + Demon
            + Mechanical
            + Beast
            + Totem
            + Pirate
            + Dragon
    + faction (optional, string) - The faction this card belongs to. If not specified, returns cards from all factions.
        + Members
            + Horde
            + Alliance
            + Neutral
    + collectible (optional, boolean) - If true, returns collectible cards. If false, returns non-collectible cards. If not specifed, returns both.
    + elite (optional, boolean) - If true, returns elite cards. If false, returns non-elite cards. If not specified, returns both.

### GET

+ Response 200 (application/json)

        {
           "EX1_009":{
              "goldImage":"http://wow.zamimg.com/images/hearthstone/cards/enus/animated/EX1_009_premium.gif",
              "collectible":true,
              "mechanics":[
                 "Enrage"
              ],
              "flavorText":"There is no beast more frightening (or ridiculous) than a fully enraged chicken.",
              "enchantmentIdleVisual":0,
              "cardSet":"Classic",
              "cardName":"Angry Chicken",
              "locale":"enUS",
              "image":"http://wow.zamimg.com/images/hearthstone/cards/enus/original/EX1_009.png",
              "enchantmentBirthVisual":0,
              "cardType":"Minion",
              "rarity":"Rare",
              "artistName":"Mike Sass",
              "cost":1,
              "health":1,
              "race":"Beast",
              "cardTextInHand":"Enrage: +5 Attack.",
              "attack":1,
              "elite":false,
              "id":"EX1_009"
           }
        }
        
## Card By ID [/v1.0/cards/by-id/{id}{?locale}{?cardType}{?cardSet}{?class}{?race}{?faction}{?rarity}{?collectible}{?elite}]

Get a card by its ID. 

You can filter by locale, cardType, cardSet, class, race, faction, and rarity using optional query parameters.
Returns card in English (locale: enUS) by default.

+ Parameters
    + id: EX1_009 (required, string) - ID of the card.
    + locale: enUS (optional, string) - Locale code for returned data. Defaults to enUS.
        + Members
            + deDE
            + enGB
            + enUS
            + esES
            + esMX
            + frFR
            + itIT
            + koKR
            + plPL
            + ptBR
            + ptPT
            + ruRU
            + zhCN
            + zhTW
    + cardSet (optional, string) - Card set. If not specified, returns all sets.
        + Members
            + Basic
            + Classic
            + Reward
            + Missions
            + System
            + Debug
            + Promotion
            + Curse of Naxxramas
            + Goblin vs Gnomes
            + Blackrock Mountain
            + Credits
    + cardType (optional, string) - Card type. If not specified, returns all types.
        + Members
            + Hero
            + Minion
            + Spell
            + Enchantment
            + Weapon
            + Hero Power
    + rarity (optional, string) - Card rarity. If not specified, returns all rarities.
        + Members
            + Developer
            + Common
            + Free
            + Rare
            + Epic
            + Legendary
    + class (optional, string) - The class this card belongs to. If not specified, returns all classes.
        + Members
            + Developer
            + Druid
            + Hunter
            + Mage
            + Paladin
            + Priest
            + Rogue
            + Shaman
            + Warlock
            + Warrior
            + Dream
    + race (optional, string) - The race of this card. If not specified, returns cards from all races.
        + Members
            + Murloc
            + Demon
            + Mechanical
            + Beast
            + Totem
            + Pirate
            + Dragon
    + faction (optional, string) - The faction this card belongs to. If not specified, returns cards from all factions.
        + Members
            + Horde
            + Alliance
            + Neutral
    + collectible (optional, boolean) - If true, returns collectible cards. If false, returns non-collectible cards. If not specifed, returns both.
    + elite (optional, boolean) - If true, returns elite cards. If false, returns non-elite cards. If not specified, returns both.
    
### GET

+ Response 200 (application/json)

        {
           "EX1_009":{
              "goldImage":"http://wow.zamimg.com/images/hearthstone/cards/enus/animated/EX1_009_premium.gif",
              "collectible":true,
              "mechanics":[
                 "Enrage"
              ],
              "flavorText":"There is no beast more frightening (or ridiculous) than a fully enraged chicken.",
              "enchantmentIdleVisual":0,
              "cardSet":"Classic",
              "cardName":"Angry Chicken",
              "locale":"enUS",
              "image":"http://wow.zamimg.com/images/hearthstone/cards/enus/original/EX1_009.png",
              "enchantmentBirthVisual":0,
              "cardType":"Minion",
              "rarity":"Rare",
              "artistName":"Mike Sass",
              "cost":1,
              "health":1,
              "race":"Beast",
              "cardTextInHand":"Enrage: +5 Attack.",
              "attack":1,
              "elite":false,
              "id":"EX1_009"
           }
        }
